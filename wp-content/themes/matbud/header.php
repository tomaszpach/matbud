<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <link href="https://fonts.googleapis.com/css?family=Lato|Montserrat|Quicksand|VT323" rel="stylesheet">
    <link rel="stylesheet" href="<?= get_template_directory_uri() ?>/assets/libs/font-awesome/css/font-awesome.min.css">
    <?php if ( is_singular() && pings_open( get_queried_object() ) ) : ?>
        <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<?php endif;
	wp_head();
	?>
</head>

<body <?php body_class(); ?>>
<?php
$featuredImage = '';
if (has_post_thumbnail()) {
	$featuredImage = get_the_post_thumbnail_url(get_the_ID());

} else {
	$featuredImage = get_template_directory_uri() . '/assets/img/background-3.jpeg';
}
?>
<header class="header container-fluid" style="background-image: url('<?= $featuredImage; ?>')">
    <div class="row">
        <div class="site-branding col">
            <div class="right-me">
                <div class="hamburger">
                    <span></span>
                    <span></span>
                    <span></span>
                </div>
            </div>
			<?php if ( is_front_page() ): ?>
            <div class="center-me sticky">
                <img class="wow fadeInUp" src="<?= get_template_directory_uri() ?>/assets/img/logo.png"
                     alt="MatBud">
            </div>
                <div class="center-me">
                    <i class="fa fa-chevron-down animated infinite bounce" aria-hidden="true"><span></span></i>
                </div>
			<?php else: ?>
                <img class="animated fadeInUp" src="<?= get_template_directory_uri() ?>/assets/img/logo-no-text.png"
                     alt="MatBud">
			<?php endif; ?>
        </div>

        <div class="mobile-menu">
	        <?php wp_nav_menu( [
		        'menu'           => 'header',
		        'theme_location' => 'header',
		        'container'      => false,
		        'menu_class'     => 'header-mobile-nav justify-content-center'
	        ] );
	        ?>
        </div>
    </div>
</header>
<?php wp_nav_menu( [
	'menu'           => 'header',
	'theme_location' => 'header',
	'container'      => false,
	'menu_class'     => 'header-main-nav justify-content-center'
] );
?>

<div id="content" class="site-content container-fluid">