<?php
/**
 * Created by PhpStorm.
 * User: tomaszpach
 * Template Name: Kontakt
 */

get_header();
?>

    <div id="contact" class="container">
        <div class="row align-items-center">
            <h2 class="text-center w-100">Kontakt</h2>
            <div class="desc col col-md-6 px-sm-0 pr-md-3">
                <p style="text-align: left;">Ready for some Hammer time? Write to us or give us a call now, we’re always
                    happy to provide a free consultation and help out with any questions you may ask.</p>

                <h3>Get in touch</h3>
                <p>1-800-000-0000 // info@mysite.com
                    500 Terry Francois St.
                    San Francisco, CA 94158</p>
            </div>
            <div class="col">
				<?= do_shortcode( '[contact-form-7 id="61" title="Kontakt"]' ) ?>
            </div>
        </div>
    </div>


    <div class="row">
        <div id="map"></div>

        <script>
            function initMap() {
                var matbud = {lat: 49.668092, lng: 20.861072};
                var map = new google.maps.Map(document.getElementById('map'), {
                    zoom: 16,
                    center: matbud
                });
                var marker = new google.maps.Marker({
                    position: matbud,
                    map: map
                });
            }
        </script>
        <script async defer
                src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBQSnJkpfDUoXUoiO1pvzfL9wZO7kDEPuQ&callback=initMap">
        </script>
    </div>

<?php get_footer(); ?>