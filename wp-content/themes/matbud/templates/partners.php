<?php
/**
 * Created by PhpStorm.
 * User: tomaszpach
 * Template Name: Partnerzy
 */

get_header();
?>

    <div class="container">
	    <?php

	    $id      = get_the_ID();
	    $post    = get_post( $id );
	    $content = apply_filters( 'the_content', $post->post_content );
	    echo $content;

	    ?>
    </div>

<?php get_footer(); ?>