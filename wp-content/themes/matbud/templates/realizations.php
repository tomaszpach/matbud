<?php
/**
 * Created by PhpStorm.
 * User: tomaszpach
 * Template Name: Realizacje
 */

get_header();
?>

	<div id="realizations" class="container">
		<?php
		$id      = get_the_ID();
		$post    = get_post( $id );
		$content = apply_filters( 'the_content', $post->post_content );
		echo $content; ?>

		<div id="accordion" role="tablist" aria-multiselectable="true">

			<?php // Single Accordion
			$args = array(
				'post_type'      => 'accordion',
				'orderby'        => 'date',
				'order'          => 'DESC',
				'posts_per_page' => - 1,
			);

			$query = new \WP_Query( $args );

			foreach ( $query->posts as $post ) {
				$ID                  = $post->ID;
				$title               = $post->post_title;
				$accordionID         = $string = str_replace( ' ', '', $title );
				$postContent         = $post->post_content;
				$postContentFiltered = apply_filters( 'the_content', $postContent );
				$imageURL            = get_the_post_thumbnail_url( $ID, $size = 'post-thumbnail' );

				$accordionFull .= '
    <div class="card">
        <div class="card-header" role="tab" id="heading' . $accordionID . '">
            <h5 class="m-0">
                <a class="p-2 w-100 figure" data-toggle="collapse" data-parent="#accordion" href="#' . $accordionID . '" aria-expanded="true"
                   aria-controls="' . $accordionID . '">
                    ' . $title . '
                </a>
            </h5>
        </div>

        <div id="' . $accordionID . '" class="collapse" role="tabpanel" aria-labelledby="heading' . $accordionID . '">
            <div class="card-block">
                ' . $postContentFiltered . '
            </div>
        </div>
    </div>   
';
			}

			echo $accordionFull;

			?>
		</div>
	</div>

<?php get_footer(); ?>