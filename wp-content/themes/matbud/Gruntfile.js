module.exports = function( grunt ) {
    "use strict"; // support for let and const

    // Load grunt tasks automatically
    require( 'load-grunt-tasks' )( grunt );

    const path = require('path');

    // Load module paths
    let modulePaths = [];
    if( grunt.file.exists( 'module-paths.json' )) {
        modulePaths = grunt.file.readJSON( 'module-paths.json' );
    } else {
        grunt.log.error([ 'File module-paths.json does\'t exists! Run local WP site to regenrate this file automatically (WP_DEBUG must be enabled).' ]);
    }

    // Load module blogs info
    let moduleBlogsPaths = {};
    for( let i = 0, n = modulePaths.length; i < n; ++i ) {
        Object.assign( moduleBlogsPaths, retrieveBlogPaths( modulePaths[ i ] ) );
    }

    function retrieveBlogPaths( modulePath ) {
        let paths = {},
            blogs = grunt.file.expand(
            {},
            modulePath + '/dynamic/_blog-*.scss'
        );
        for( let j = 0, m = blogs.length; j < m; ++j ) {
            let parsed = path.parse( blogs[ j ] ),
                file = parsed.name.substr( 1 ),
                module = path.normalize( path.join( parsed.dir, '/..' )),
                from = path.join( module, 'scss', 'module.scss' ),
                to = path.join( module, 'dynamic', file + '.css' );

            paths[ to ] = from;
        }

        return paths;
    }

    let getModulePaths = function( suffix = '' ) {
        let paths = [];

        if( modulePaths ) {
            modulePaths.forEach( function( modulePath ) {
                paths.push( path.join( modulePath, suffix ));
            });
        }

        return paths;
    };

    let modulesScssPaths = (function() {
        let paths = {},
            modulePaths = getModulePaths();

        if( modulePaths ) {
            modulePaths.forEach( function( modulePath ) {
                grunt.file.expandMapping(
                    modulePath,
                    '',
                    { ext: '/dynamic/module.css' }
                ).forEach( function( item ) {
                    let scss = path.join( item.src[ 0 ], 'scss/module.scss' );
                    if( grunt.file.exists( scss )) {
                        paths[ item.dest ] = scss;
                    } else {
                        grunt.log.error([ 'Skipped from compilation, file "scss/module.scss" not exist in "' + item.src[ 0 ] + '" module!' ]);
                    }
                });
            });
        }

        return paths;
    })();

    function blogImporter(url, prev, done) {
        if( url === '../dynamic/main' ) {
            let name = path.basename( this.options.outFile, '.css' ),
                dataSource = path.join( path.dirname( this.options.file ), '..', 'dynamic', '_' + name + '.scss' );
            done( { file: dataSource } );
        } else
            done( { file: url } );
    }

    let watchedModuleStyle = getModulePaths( 'scss/**/*.scss' );
    let watchedModuleDynamicFiles = getModulePaths( 'dynamic/_blog-*.scss' );

    // Project configuration.
    grunt.initConfig( {
        sass: {
            options: {
                compass: false,
                sourceMap: true,
                outputStyle: 'nested', // nested, expanded, compact, compressed
                sourceComments: false
            },
            fonts: {
                files: {
                    'assets/stylesheets/css/fonts.css': 'assets/stylesheets/scss/fonts.scss'
                }
            },
            bootstrap: {
                files: {
                    'assets/stylesheets/css/bootstrap.css': 'assets/stylesheets/scss/bootstrap.scss'
                }
            },
            style: {
                files: {
                    'assets/stylesheets/css/style.css': 'assets/stylesheets/scss/style.scss'
                }
            },
            wpEditor: {
                files: {
                    'assets/stylesheets/css/wp-editor.css': 'assets/stylesheets/scss/wp-editor.scss'
                }
            },
            singleModule: {
                options: {
                    vars: { compile_overall: true },
                    includePaths: [ 'assets/stylesheets/scss' ]
                }
            },
            allModules: {
                options: {
                    vars: { compile_overall: true },
                    includePaths: [ 'assets/stylesheets/scss' ]
                },
                files: modulesScssPaths
            },
            allBlogs: {
                options: {
                    vars: { compile_loop: true },
                    importer: blogImporter,
                    includePaths: [ 'assets/stylesheets/scss' ]
                },
                files: moduleBlogsPaths
            }
        },
        watch: {
            options: {
                spawn: false
            },
            gruntfile: {
                files: [
                    'Gruntfile.js',
                    'module-paths.json'
                ],
                options: {
                    reload: true
                }
            },
            sass: {
                files: [
                    'assets/stylesheets/scss/style.scss',
                    'assets/stylesheets/scss/mixins/**/*.scss',
                    'assets/stylesheets/scss/partials/**/*.scss'
                ],
                tasks: [ 'sass:style', 'postcss:style' ]
            },
            sassBootstrap: {
                files: 'assets/stylesheets/scss/bootstrap.scss',
                tasks: [ 'sass:bootstrap', 'postcss:bootstrap' ]
            },
            sassFonts: {
                files: 'assets/stylesheets/scss/fonts.scss',
                tasks: [ 'sass:fonts', 'postcss:fonts' ]
            },
            sassWpEditor: {
                files: 'assets/stylesheets/scss/wp-editor.scss',
                tasks: [ 'sass:wpEditor', 'postcss:wpEditor' ]
            },
            sassThemeConfig: {
                files: 'assets/stylesheets/scss/_theme-config.scss',
                tasks: [ 'sass:style', 'postcss:style', 'sass:bootstrap', 'postcss:bootstrap' ]
            },
            sassModuleConfig: {
                files: 'assets/stylesheets/scss/_module-config.scss',
                tasks: [ 'sass:allModules', 'postcss:allModules', 'sass:allBlogs' ]
            },
            sassVendor: {
                files: 'assets/stylesheets/vendor-config.json',
                tasks: [ 'install-sass-vendor' ]
            },
            bowerVendor: {
                files: 'assets/vendors/vendor-config.json',
                tasks: [ 'install-bower-vendor' ]
            },
            sassModule: {
                files: watchedModuleStyle,
                tasks: [ 'sass:singleModule', 'postcss:singleModule', 'sass:allBlogs' ]
            },
            sassModuleBlog: {
                files: watchedModuleDynamicFiles,
                options: {
                    event: ['added', 'changed']
                },
                tasks: [ 'sass:allBlogs' ]
            },
            sassModuleBlogDeleted: {
                files: watchedModuleDynamicFiles,
                options: {
                    event: ['deleted']
                }
            }
        },
        auto_install: {
            local: {
                options: {
                    stdout: true,
                    stderr: true,
                    failOnError: true,
                    npm: false
                }
            }
        },
        copy: {
            sassVendor: {
                files: [] // Initialized in task
            },
            bowerVendor: {
                files: [] // Initialized in task
            }
        },
        clean: {
            sassVendor: ["assets/stylesheets/scss/vendor/"],
            bowerVendor: ["assets/vendors/*/"]
        },
        concat: {
            theme: {
                src: [
                    'assets/stylesheets/css/fonts.css',
                    'assets/stylesheets/css/bootstrap.css',
                    'assets/stylesheets/css/style.css',
                ],
                dest: 'assets/stylesheets/css/theme.css'
            },
            modules: {
                src: grunt.util._.keys( modulesScssPaths ),
                dest: 'assets/stylesheets/css/modules.css'
            }
        },
        cssnano: {
            options: {
                sourcemap: false,
                zindex: false,
                reduceIdents: false,
                mergeIdents: false,
                discardUnused: false
            },
            theme: {
                files: {
                    'assets/stylesheets/css/theme.min.css': 'assets/stylesheets/css/theme.css'
                }
            },
            modules: {
                files: {
                    'assets/stylesheets/css/modules.min.css': 'assets/stylesheets/css/modules.css'
                }
            },
            wpEditor: {
                files: {
                    'assets/stylesheets/css/wp-editor.min.css': 'assets/stylesheets/css/wp-editor.css'
                }
            }
        },
        postcss: {
            options: {
                map: {
                    inline: false,
                    annotation: true,
                    sourcesContent: true
                },
                processors: [
                    require('postcss-flexbugs-fixes')(), // from official bootstrap compile code
                    require('autoprefixer')({ browsers: 'last 2 versions' }), // add vendor prefixes
                ]
            },
            fonts: {
                src: 'assets/stylesheets/css/fonts.css'
            },
            bootstrap: {
                src: 'assets/stylesheets/css/bootstrap.css'
            },
            style: {
                src: 'assets/stylesheets/css/style.css'
            },
            wpEditor: {
                src: 'assets/stylesheets/css/wp-editor.css'
            },
            singleModule: {},
            allModules: {
                src: grunt.util._.keys( modulesScssPaths )
            }
        }
    } );

    grunt.event.on('watch', function(action, filepath, target) {
        // Run sass compiler only for changed module
        if( target === 'sassModule' ) {
            let files = {},
                to = path.join( path.dirname( filepath ), '..', 'dynamic', 'module.css' );

            files[ to ] = filepath;

            // Recompile overall_module
            grunt.config( 'sass.singleModule.files', files );
            // Use Autoprefixer for overall_module
            grunt.config( 'postcss.singleModule.src', to );

            // Recompile blogs in module
            let modulePath = path.dirname( path.dirname( filepath ));
            grunt.config( 'sass.singleModuleBlogs.files', retrieveBlogPaths( modulePath ));
        }
        // Clear all events after reload Gruntfile.
        else if( target === 'gruntfile' ) {
            grunt.event.removeAllListeners();
        }
        else if( target === 'sassModuleBlogDeleted' ) {
            // Delete unused css files
            const regex = /_(blog-\d+)\.scss$/;
            const oldFile = filepath.replace( regex, '$1.css' );
            const oldFileMap = oldFile + '.map';

            if( grunt.file.exists( oldFile ) ) {
                grunt.log.writeln( [ '>> Deleting unused file "' + oldFile + '".' ] );
                grunt.file.delete( oldFile, { force: true } );
            }
            if( grunt.file.exists( oldFileMap ) ) {
                grunt.log.writeln( [ '>> Deleting unused file "' + oldFileMap + '".' ] );
                grunt.file.delete( oldFileMap, { force: true } );
            }
        }
    });

    grunt.registerTask( 'install-sass-vendor', 'Install vendors from bower and copy to vendor folder', function() {
        grunt.config.merge({
            copy: {
                sassVendor: {
                    files: grunt.file.readJSON( 'assets/stylesheets/vendor-config.json' )
                }
            }
        });
        grunt.task.run([
            'auto_install',
            'clean:sassVendor',
            'copy:sassVendor'
        ]);
    });

    grunt.registerTask( 'install-bower-vendor', 'Install libs from bower and copy to libs folder', function() {
        grunt.config.merge({
            copy: {
                bowerVendor: {
                    files: grunt.file.readJSON( 'assets/vendors/vendor-config.json' )
                }
            }
        });
        grunt.task.run([
            'auto_install',
            'clean:bowerVendor',
            'copy:bowerVendor'
        ]);
    });

    grunt.registerTask( 'dev', 'Generate files for development', [
        'install-sass-vendor',
        'install-bower-vendor',
        'sass:fonts',
        'postcss:fonts',
        'sass:bootstrap',
        'postcss:bootstrap',
        'sass:style',
        'postcss:style',
        'sass:wpEditor',
        'postcss:wpEditor',
        'sass:allModules',
        'postcss:allModules',
        'sass:allBlogs'
    ] );

    grunt.registerTask( 'prod', 'Generate files for production', [
        'dev',
        'concat',
        'cssnano'
    ] );

    grunt.registerTask( 'default', 'Watch scss files and compile after change', [
        'watch'
    ] );

};