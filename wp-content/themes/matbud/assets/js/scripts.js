jQuery(function ($) {
    $('.slider').slick({
        autoplay: true,
        autoplaySpeed: 1500,
        arrows: false,
        centerMode: true,
        centerPadding: '100px',
        dots: false,
        slidesToShow: 1,
        speed: 3000
    });
    $('.gallery-slider').slick({});

    // AOS not working
    // AOS.init({
    //     offset: 200,
    //     duration: 600,
    //     easing: 'ease-in-sine',
    //     delay: 100
    // });

    new WOW().init();

    // Przewijanie całą wysokość brandingu po kliknięciu
    $("i.fa").click(function () {
        $("html, body").animate({scrollTop: $('.site-branding').height()}, "slow");
        return false;
    });

    // Sprawdzanie czy menu jest już sticky
    $(window).scroll(function () {
        // console.log($('#menu-header-navigation').offset().top - $(window).scrollTop());
    });

    // Hamburger
    $('.hamburger').click(function () {
        $('body').toggleClass('mobile-menu-open');
    });
});