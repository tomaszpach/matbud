<?php
/**
 * Libs folder is for all external libraries (css, js, etc.)
 *
 * This file contains only wp_register_script() and wp_register_style() for vendor libs.
 * For enqueue this, use functions.php or pagebox module config.
 */

/**
 * Unregister WordPress jQuery
 */
wp_deregister_script( 'jquery' );

/**
 * JS files
 */
wp_register_script( 'jquery', THEME_VENDORS_URI . '/jquery/jquery.min.js', [], '3.2.1', true );
//wp_register_script( 'aos', THEME_VENDORS_URI . '/aos/aos.js', [ 'jquery' ], '2.1.1', true ); // not working
wp_register_script( 'wow', THEME_VENDORS_URI . '/wow/wow.js', [ 'jquery' ], '2.1.1', true );
wp_register_script( 'tether', THEME_VENDORS_URI . '/tether/tether.min.js', [ 'jquery' ], '1.3.7', true );
wp_register_script( 'bootstrapjs', THEME_VENDORS_URI . '/bootstrap/bootstrap.min.js', [ 'jquery', 'tether' ], 'v4.0.0-alpha.6', true );
wp_register_script( 'slick', THEME_VENDORS_URI . '/slick/slick.min.js', [ 'jquery' ], '1.8.0', true );
wp_register_script( 'scripts', THEME_ASSETS . 'js/scripts.js', ['jquery'], '1.0.0', true );



/**
 * CSS files
 */
//wp_register_style( 'aos', THEME_VENDORS_URI . '/aos/aos.css', [], '2.1.1', true ); // not working
wp_register_style( 'slick', THEME_LIBS_URI . 'slick/slick.css', [], '1.8.0' );
wp_register_style( 'slick-theme', THEME_LIBS_URI . 'slick/slick-theme.css', [], '1.8.0' );
wp_register_style( 'animateme', THEME_LIBS_URI . 'animateme/animate.css', [], '1.8.0' );