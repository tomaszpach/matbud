<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Matbud
 * @since Matbud 1.0
 */

get_header(); ?>

<div class="container text-center">
    <?php

    $id      = get_the_ID();
    $post    = get_post( $id );
    $content = apply_filters( 'the_content', $post->post_content );
    echo $content;

    ?>
</div>


<?php get_footer(); ?>


