<?php
/**
 * Created by PhpStorm.
 * User: tomaszpach
 * Date: 21.07.2017
 * Time: 20:13
 */

/**
 * Global definitions
 */
require_once 'functions/global-definitions.php';

/**
 * Import Theme Utils
 */
require_once 'functions/theme.php';

/**
 * Theme styles (CSS) and scripts (JavaScript)
 * Use registered media in assets/vendors/register_vendors.php
 */

add_action('wp_enqueue_scripts', function () {

	wp_enqueue_script( 'jquery' );
//	wp_enqueue_script( 'aos' ); // Not working
	wp_enqueue_script( 'wow' );
	wp_enqueue_script( 'slick' );
	wp_enqueue_script( 'tether' );
	wp_enqueue_script( 'bootstrapjs' );

	wp_enqueue_style( 'aos' );
	wp_enqueue_style( 'slick' );
	wp_enqueue_style( 'slick-theme' );
	wp_enqueue_style( 'animateme' );

	wp_enqueue_script( 'scripts' );
	wp_localize_script('scripts', 'Ajax', ['ajax_url' => admin_url('admin-ajax.php')]);
});

/**
 * Register default menus
 */
add_action( 'after_setup_theme', function () {
	register_nav_menus( [
		'header' => __( 'Header Menu', 'theme' ),
		'footer' => __( 'Footer Menu', 'theme' )
	] );
} );


add_theme_support( 'post-thumbnails' );


// Register Custom Post Type
function accordion() {

	$labels = array(
		'name'                  => _x( 'Accordions', 'Post Type General Name', 'accordion' ),
		'singular_name'         => _x( 'Accordion', 'Post Type Singular Name', 'accordion' ),
		'menu_name'             => __( 'Accordion', 'accordion' ),
		'name_admin_bar'        => __( 'Accordion', 'accordion' ),
		'archives'              => __( 'Accordion Archives', 'accordion' ),
		'attributes'            => __( 'Accordion Attributes', 'accordion' ),
		'parent_item_colon'     => __( 'Parent Accordion:', 'accordion' ),
		'all_items'             => __( 'All Accordions', 'accordion' ),
		'add_new_item'          => __( 'Add New Accordion', 'accordion' ),
		'add_new'               => __( 'Add New', 'accordion' ),
		'new_item'              => __( 'New Accordion', 'accordion' ),
		'edit_item'             => __( 'Edit Accordion', 'accordion' ),
		'update_item'           => __( 'Update Accordion', 'accordion' ),
		'view_item'             => __( 'View Accordion', 'accordion' ),
		'view_items'            => __( 'View Accordions', 'accordion' ),
		'search_items'          => __( 'Search Accordion', 'accordion' ),
		'not_found'             => __( 'Not found', 'accordion' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'accordion' ),
		'featured_image'        => __( 'Featured Image', 'accordion' ),
		'set_featured_image'    => __( 'Set featured image', 'accordion' ),
		'remove_featured_image' => __( 'Remove featured image', 'accordion' ),
		'use_featured_image'    => __( 'Use as featured image', 'accordion' ),
		'insert_into_item'      => __( 'Insert into Accordion', 'accordion' ),
		'uploaded_to_this_item' => __( 'Uploaded to this Accordion', 'accordion' ),
		'items_list'            => __( 'Accordions list', 'accordion' ),
		'items_list_navigation' => __( 'Accordions list navigation', 'accordion' ),
		'filter_items_list'     => __( 'Filter Accordions list', 'accordion' ),
	);
	$args = array(
		'label'                 => __( 'Accordion', 'accordion' ),
		'description'           => __( 'Show and hide content', 'accordion' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'thumbnail', 'custom-fields' ),
		'taxonomies'            => array( 'category', 'post_tag' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'menu_icon'             => 'dashicons-editor-justify',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => false,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => true,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type( 'accordion', $args );

}
add_action( 'init', 'accordion', 0 );


// Register Custom Post Type
function sliders() {

	$labels = array(
		'name'                  => _x( 'Sliders', 'Post Type General Name', 'slider' ),
		'singular_name'         => _x( 'Slider', 'Post Type Singular Name', 'slider' ),
		'menu_name'             => __( 'Slider', 'slider' ),
		'name_admin_bar'        => __( 'Slider', 'slider' ),
		'archives'              => __( 'Slider Archives', 'slider' ),
		'attributes'            => __( 'Slider Attributes', 'slider' ),
		'parent_item_colon'     => __( 'Parent Slider:', 'slider' ),
		'all_items'             => __( 'All Sliders', 'slider' ),
		'add_new_item'          => __( 'Add New Slider', 'slider' ),
		'add_new'               => __( 'Add New', 'slider' ),
		'new_item'              => __( 'New Slider', 'slider' ),
		'edit_item'             => __( 'Edit Slider', 'slider' ),
		'update_item'           => __( 'Update Slider', 'slider' ),
		'view_item'             => __( 'View Slider', 'slider' ),
		'view_items'            => __( 'View Sliders', 'slider' ),
		'search_items'          => __( 'Search Slider', 'slider' ),
		'not_found'             => __( 'Not found', 'slider' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'slider' ),
		'featured_image'        => __( 'Featured Image', 'slider' ),
		'set_featured_image'    => __( 'Set featured image', 'slider' ),
		'remove_featured_image' => __( 'Remove featured image', 'slider' ),
		'use_featured_image'    => __( 'Use as featured image', 'slider' ),
		'insert_into_item'      => __( 'Insert into Slider', 'slider' ),
		'uploaded_to_this_item' => __( 'Uploaded to this Slider', 'slider' ),
		'items_list'            => __( 'Sliders list', 'slider' ),
		'items_list_navigation' => __( 'Sliders list navigation', 'slider' ),
		'filter_items_list'     => __( 'Filter Sliders list', 'slider' ),
	);

	$args = array(
		'label'                 => __( 'Slider', 'slider' ),
		'description'           => __( 'Show and hide content', 'slider' ),
		'labels'                => $labels,
		'supports'              => array( 'title' ),
		'taxonomies'            => array( 'category', 'post_tag' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'menu_icon'             => 'dashicons-images-alt2',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => false,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => true,
		'publicly_queryable'    => true,
		'capability_type'       => 'post'
	);
	register_post_type( 'sliders', $args );

}
add_action( 'init', 'sliders', 0 );


// Add Shortcode
function slider( $atts ) {
	// Attributes
	$atts = shortcode_atts(
		array(
			'id' => '',
		),
		$atts,
		'slider'
	);

	$id = $atts['id'];

	$html = '<div class="slider">';
	$entries = get_post_meta( $id, 'sliders_group', true );

	foreach ( (array) $entries as $key => $entry ) {
		$img = '';
		if ( isset( $entry['image_id'] ) ) {
			$img = wp_get_attachment_image_url( $entry['image_id'], 'share-pick', null, array(
				'class' => 'thumb',
			) );

			$html .= <<<IMG
<div class="wrapper" style="background-image: url('$img');"></div>
IMG;
		}
	}

	$html .= '</div>';
	return $html;

}
add_shortcode( 'slider', 'slider' );





add_action( 'cmb2_admin_init', 'cm2_sliders' );
/**
 * Define the metabox and field configurations.
 */
function cm2_sliders() {
	// Start with an underscore to hide fields from custom fields list
	$prefix = '_slider_';

	/**
	 * Initiate the metabox
	 */
	$cmb = new_cmb2_box( array(
		'id'            => 'slider',
		'title'         => __( 'Slider box', 'cmb2' ),
		'object_types'  => array( 'sliders' ), // Post type
		'context'       => 'normal',
		'priority'      => 'low',
		'show_names'    => true, // Show field names on the left
		 'cmb_styles' => true, // false to disable the CMB stylesheet
		// 'closed'     => true, // Keep the metabox closed by default
	) );


	$group_field_id = $cmb->add_field( array(
		'id'          => 'sliders_group',
		'type'        => 'group',
		'description' => __( 'Generates reusable form entries', 'cmb2' ),
		// 'repeatable'  => false, // use false if you want non-repeatable group
		'options'     => array(
			'group_title'   => __( 'Slajd {#}', 'cmb2' ), // since version 1.1.4, {#} gets replaced by row number
			'add_button'    => __( 'Add Another Entry', 'cmb2' ),
			'remove_button' => __( 'Remove Entry', 'cmb2' ),
			'sortable'      => true, // beta
			// 'closed'     => true, // true to have the groups closed by default
		),
	) );

	// Id's for group's fields only need to be unique for the group. Prefix is not needed.
	$cmb->add_group_field( $group_field_id, array(
		'name' => 'Wybierz obrazek',
		'id'   => 'image',
		'type' => 'file',
	) );
}