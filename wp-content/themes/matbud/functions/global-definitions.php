<?php

/**
* Defined constants
*/
define( 'THEME_DIR_PATH',       get_template_directory() );
define( 'THEME_DIR_URI',        get_template_directory_uri() );

/**
 * Assets constants
 */
define( 'THEME_IMAGE_URI',        THEME_DIR_URI  . '/assets/img/' );
define( 'THEME_ASSETS',        THEME_DIR_URI  . '/assets/' );
define( 'THEME_LIBS_URI',        THEME_DIR_URI  . '/assets/libs/' );
define( 'THEME_VENDORS',        THEME_DIR_PATH  . '/assets/vendors' );
define( 'THEME_VENDORS_URI',    THEME_DIR_URI   . '/assets/vendors' );

define( 'THEME_SCSS',           THEME_DIR_PATH  . '/assets/stylesheets/scss' );
define( 'THEME_STYLE_URI',        THEME_DIR_URI   . '/assets/stylesheets/' );
define( 'THEME_JS_URI',         THEME_DIR_URI   . '/assets/javascripts' );
define( 'THEME_FONTS_URI',      THEME_DIR_URI   . '/assets/fonts' );
define( 'THEME_IMAGES_URI',     THEME_DIR_URI   . '/assets/images' );

define( 'HOME_URL', get_home_url() );