<?php

/**
 * Theme autoload media (CSS & JS)
 */
add_action( 'wp_enqueue_scripts', function () {
	/**
	 * Register vendors (required).
	 */
	include THEME_VENDORS . '/register_vendors.php';

	/**
	 * Load css styles for development.
	 */
	$version_hash = uniqid();
	wp_enqueue_style( 'bootstrap', THEME_STYLE_URI . 'bootstrap.css', [], $version_hash, 'all' );
	wp_enqueue_style( 'typography', THEME_STYLE_URI . 'typography.css', [], $version_hash, 'all' );
	wp_enqueue_style( 'style', THEME_STYLE_URI . 'style.css', [], $version_hash, 'all' );

} );