<?php
/**
 * Created by PhpStorm.
 * User: tomaszpach
 * Date: 21.07.2017
 * Time: 18:36
 */ ?>

</div> <!-- // #content -->
<?php wp_footer(); ?>
</body>
<footer class="text-center">
	<div class="container">
		<div class="row">
			<div class="wrapper">
				<img src="<?= THEME_IMAGE_URI; ?>logo-no-text.png" alt="footer image">
			</div>
		</div>

		<div class="row disclaimer">
			<p>MATBUD Spółka Cywilna Bogdan Matusik Damian Matusik</p>
			<p>ul. Jana Długosza 72, 33-300 Nowy Sącz</p>
			<p>NIP: 734-354-27-50, REGON: 365514489</p>
		</div>
	</div>

	<div class="container-fluid">
		<div class="mobile-contact row w-100">
			<a class="phone col-6 align-items-center d-flex justify-content-center" href="tel:48535155345">
				<i class="fa fa-phone"
				   aria-hidden="true"></i>
			</a>
			<a class="mail col-6 align-items-center d-flex justify-content-center" href="mailto:matusik@matusik.pl">
				<i class="fa fa-envelope"
				   aria-hidden="true"></i>
			</a>
		</div>
	</div>
</footer>